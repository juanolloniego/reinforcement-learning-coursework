#!/usr/bin/perl 

use File::Find;


my $LSG_VERSION=$ENV{"LSG_VERSION"};

my @fileArray = ();

sub appendToArray(){
  if ($_=~/\.java$/){
    push(@fileArray,$File::Find::name);
  }
} 

sub getJavaFiles{
  my $directory = shift(@_);
  @fileArray = ();
  find(\&appendToArray,($directory));
  my $result = join(' ',@fileArray);
  @fileArray = ();
  $result;
}

my $libdir="lib";

sub compileBot{
  my $directory = shift(@_);
  my $libname = shift(@_);
  print "Compiling $directory\n";
  if (!(-e "$directory/bin")){
    mkdir "$directory/bin";
  }
  my $ sourceFiles = getJavaFiles("$directory/src");
  system("javac -cp $directory/src:$libdir/lemonade$LSG_VERSION.jar $sourceFiles -d $directory/bin 2>> error.txt");
  if (-e "$directory/data"){
    system("cp -rf $directory/data/* $directory/bin");
  }
  system("jar -cf $libdir/$libname -C $directory/bin .");
}

sub cleanBot{
  my $directory = shift(@_);
  my $libname = shift(@_);
  print "Cleaning $directory\n";
  system("rm -rf $directory/bin");
  system("rm -rf $libdir/$libname");
}

sub compileBots{
  my $botRef = shift(@_);
  my $dir = shift(@_);
  my %bots = %{$botRef};
  while(my ($directory,$libname) = each(%bots)){
    my $fullDir = $dir.$directory;
    compileBot($fullDir,$libname);
  }
}

sub cleanBots{
  my $botRef = shift(@_);
  my $dir = shift(@_);
  my %bots = %{$botRef};
  while(my ($directory,$libname) = each(%bots)){
    my $fullDir = $dir.$directory;
    cleanBot($fullDir,$libname);
  }
}
#Morrill
#rggibson

1;

