package com.yahoo.lsg.lifelong.mybots;

import com.yahoo.lsg.lifelong.GameInformation;
import game.Game;
import game.Utility;
import com.yahoo.lsg.lifelong.LifelongStrategy;
import lemonade.LemonadeUtility;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;

public class FplBot implements LifelongStrategy {

    private Utility gameUtil;
    private int numActions;
    private Random rand;
    private int lambda = 1;  // Exponential distribution parameter

    private double[] armsTracker;

    private String filename = "/home/juan/Desktop/MSC/Semester 2/Reinforcement Learning/coursework/framework/outs/FplBot/";
    private PrintStream stream;
    private int gameCount = 0;

    public FplBot(GameInformation info, long seed, String options){
        this.rand = new Random(seed);
    }

    @Override
    public void newGame(Game g, int iterations) {
        this.numActions = g.getNumActions();
        this.gameUtil = new LemonadeUtility(numActions);
        this.armsTracker = new double[numActions];
        try {
            String currentFile = filename + "FplBot" + gameCount +".txt";
            this.stream = new PrintStream(new FileOutputStream(currentFile));

        } catch (Exception e) {
            System.out.println("Exception file problem");
        }
        this.gameCount++;
    }

    @Override
    public void observeOutcome(int[] actions) {
        int[] utils = gameUtil.getUtility(actions);
        String line = Arrays.toString(utils).replaceAll("\\s", "");
        stream.println(line.substring(1, line.length() -1));
        //for (int i = 0; i < actions.length; i++) {
        this.armsTracker[actions[0]] += utils[0];
        //}
    }

    @Override
    public int getAction() {
        return fplActionSearch();
    }

    private int fplActionSearch() {
        int armToPull = 0;
        double maxValue = -90;
        for (int i=0; i<armsTracker.length; i++) {
            this.armsTracker[i] += getNextExponential();

            if (armsTracker[i] > maxValue) {
                armToPull = i;
                maxValue = armsTracker[i];
            }
        }

        return armToPull;
    }

    private double getNextExponential() {
        return  Math.log(1-rand.nextDouble())/(-lambda);
    }

}