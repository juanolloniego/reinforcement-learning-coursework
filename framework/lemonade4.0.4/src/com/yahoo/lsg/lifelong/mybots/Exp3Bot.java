package com.yahoo.lsg.lifelong.mybots;

import com.yahoo.lsg.lifelong.GameInformation;
import game.Game;
import game.Utility;
import com.yahoo.lsg.lifelong.LifelongStrategy;
import lemonade.LemonadeUtility;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;

public class Exp3Bot implements LifelongStrategy {

    private Utility gameUtil;
    private int numActions;
    private Random rand;
    private double gamma = 0.2;  // Exploration parameter

    private double[] armsTracker;

    private String filename = "/home/juan/Desktop/MSC/Semester 2/Reinforcement Learning/coursework/framework/outs/Exp3Bot/";
    private PrintStream stream;
    private int gameCount = 0;

    public Exp3Bot(GameInformation info, long seed, String options) {
        this.rand = new Random(seed);
    }

    @Override
    public void newGame(Game g, int iterations) {
        this.numActions = g.getNumActions();
        this.gameUtil = new LemonadeUtility(numActions);
        this.armsTracker = new double[numActions];

        // Initialise arms values
        for (int i = 0; i < armsTracker.length; i++) {
            this.armsTracker[i] = 1.0;
        }

        try {
            String currentFile = filename + "Exp3Bot" + gameCount +".txt";
            this.stream = new PrintStream(new FileOutputStream(currentFile));

        } catch (Exception e) {
            System.out.println("Exception file problem");
        }
        this.gameCount++;
    }

    @Override
    public void observeOutcome(int[] actions) {
        int[] utils = gameUtil.getUtility(actions);
        String line = Arrays.toString(utils).replaceAll("\\s", "");
        stream.println(line.substring(1, line.length() -1));
        double[] probs = getDistribution();

        int choice = actions[0];
        int reward = utils[0];

        double estimatedReward = 1.0 * reward / probs[choice];
        this.armsTracker[choice] *= Math.exp(estimatedReward * gamma / numActions);
    }


    @Override
    public int getAction() {
        return draw();
    }

    private int sumArms() {
        int value = 0;
        for (int i = 0; i < armsTracker.length; i++) {
            value += armsTracker[i];
        }
        return value;
    }

    private double[] getDistribution() {
        int sum = sumArms();
        double[] result = new double[numActions];

        for (int i = 0; i < result.length; i++) {
            result[i] = ((1 - gamma) * (armsTracker[i] / sum) + (gamma / numActions));
        }

        return result;
    }

    private int draw() {
        double choice = sumArms() * rand.nextDouble();
        int index = 0;

        for (int i = 0; i < armsTracker.length; i++) {
            choice -= armsTracker[i];
            if (choice <= 0)
                return index;

            index++;
        }

        return rand.nextInt(numActions);
    }
}
