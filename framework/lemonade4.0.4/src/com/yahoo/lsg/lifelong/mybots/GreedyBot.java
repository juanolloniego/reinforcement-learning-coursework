package com.yahoo.lsg.lifelong.mybots;

import com.yahoo.lsg.lifelong.GameInformation;
import com.yahoo.lsg.lifelong.LifelongStrategy;
import game.Game;
import game.Utility;
import lemonade.LemonadeUtility;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;

public class GreedyBot implements LifelongStrategy {

    private int numActions;
    private double[] armsTracker;
    private Utility gameUtil;
    private Random rand;

    private int lastUtil;
    private boolean explore;

    private String filename = "/home/juan/Desktop/MSC/Semester 2/Reinforcement Learning/coursework/framework/outs/GreedyBot/";
    private PrintStream stream;
    private int gameCount = 0;

    public GreedyBot(GameInformation info, long seed, String options) {
        this.rand = new Random(seed);

    }

    @Override
    public void newGame(Game g, int iterations) {
        this.numActions = g.getNumActions();
        this.armsTracker = new double[numActions];
        this.gameUtil = new LemonadeUtility(numActions);
        this.lastUtil = 0;
        this.explore = false;
        try {
            String currentFile = filename + "GreedyBot" + gameCount +".txt";
            this.stream = new PrintStream(new FileOutputStream(currentFile));

        } catch (Exception e) {
            System.out.println("Exception file problem");
        }
        this.gameCount++;
    }

    @Override
    public void observeOutcome(int[] actions) {
        int[] utils = gameUtil.getUtility(actions);
        String line = Arrays.toString(utils).replaceAll("\\s", "");
        stream.println(line.substring(1, line.length() -1));
        this.lastUtil = utils[0];
        for (int i = 0; i < utils.length; i++) {
            armsTracker[actions[i]] += utils[i] / numActions;
            if (i > 0 && utils[i] > lastUtil) {
                this.explore = true;
            }
        }
    }

    @Override
    public int getAction() {
        int index = rand.nextInt(numActions);
        if (this.explore)            this.explore = false;
        else {
            double maxValue = 0;
            for (int i = 0; i < armsTracker.length; i++) {
                if (armsTracker[i] > maxValue) {
                    maxValue = armsTracker[i];
                    index = i;
                }
            }
        }
        return index;
    }

}
