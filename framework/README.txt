INSTRUCTIONS

This has been tested only on Bash on Ubuntu on Windows; Refer to this guide of how to acquire this bash https://docs.microsoft.com/en-us/windows/wsl/install-win10. If you've installed Ubuntu properly (instead of using it on Windows), then these instructions should work fine.

After you've installed this bash, open it and update to the latest via command 'sudo apt-get update' and then 'sudo apt-get upgrade'.

Make sure you install the following: perl and default-jdk. These can be installed by run the following commands:
 sudo apt-get install perl
 sudo apt-get install default-jdk

Locate and change working directory to where you put your uncompressed files (e.g. /mnt/c/......./COMP6247_contest/)

Then run all of the following commands. Note that there should have no error at any step.
 source source.sh
 cd lemonade4.0.4/src/
 javac -d ../bin/ **/*.java
 javac -d ../bin/ **/**/*.java
 javac -d ../bin/ com/yahoo/lsg/correlatedresults/*.java
 javac -d ../bin/ com/yahoo/lsg/stats/**/*.java
 javac -d ../bin/ com/yahoo/lsg/sundisplay/*.java
 javac -d ../bin/ com/yahoo/lsg/lifelong/*.java
 javac -d ../bin/ com/yahoo/lsg/lifelong/**/*.java
 cd ../bin
 jar cvf ../../lib/lemonade$LSG_VERSION.jar **/*.class **/**/*.class com/yahoo/lsg/**/*.class com/yahoo/lsg/**/**/*.class 
 cd ../..
 perl compile.pl
 cd lib
 
Then finally run ../run2019.sh to run this year's competition. You will see an output similar to the following.

Player:com.yahoo.lsg.lifelong.examples.ExternalRegretMatchingStrategy Utility:-3.9079509999999966 StandardDeviation:0.1344016069995561
        Difference:0.2137680000000013
Player:com.yahoo.lsg.lifelong.examples.ExternalRegretMatchingStrategy Utility:-3.6941829999999953 StandardDeviation:0.13477461140508046
        Difference:11.296316999999991
Player:com.yahoo.lsg.lifelong.examples.ConstantLifelongStrategy Utility:7.602133999999996 StandardDeviation:0.21805991620791018

If you got this, then the installation is completed.
