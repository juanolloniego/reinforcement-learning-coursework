source source.sh
cd lemonade4.0.4/src/
javac -d ../bin/ **/*.java
javac -d ../bin/ **/**/*.java
javac -d ../bin/ com/yahoo/lsg/correlatedresults/*.java
javac -d ../bin/ com/yahoo/lsg/stats/**/*.java
javac -d ../bin/ com/yahoo/lsg/sundisplay/*.java
javac -d ../bin/ com/yahoo/lsg/lifelong/*.java
javac -d ../bin/ com/yahoo/lsg/lifelong/**/*.java
cd ../bin
jar cvf ../../lib/lemonade$LSG_VERSION.jar **/*.class **/**/*.class com/yahoo/lsg/**/*.class com/yahoo/lsg/**/**/*.class 
cd ../..
perl compile.pl
cd lib
../competition.sh


